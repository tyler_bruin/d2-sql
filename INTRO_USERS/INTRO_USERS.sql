/*
CREATE DATABASE INTRO_USERS
GO

USE INTRO_USERS
GO 

Tyler Bruin - 9997861
*/
-- Create Tables
CREATE TABLE Admin (
	UserName varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	email varchar(50) NOT NULL
	)

CREATE TABLE Type (
	Name varchar(50) NOT NULL PRIMARY KEY,
	Cost varchar(20) NOT NULL,
	Hours int NOT NULL
	)

CREATE TABLE Document (
	ID int NOT NULL PRIMARY KEY,
	Date date NOT NULL,
	Link varchar(50) NOT NULL,
	Type varchar(50) NOT NULL
	)

CREATE TABLE Instructor (
	Username varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	phone varchar(50) NOT NULL
	)

CREATE TABLE Car (
	License varchar(50) NOT NULL PRIMARY KEY,
	Make varchar(50) NOT NULL
	)

CREATE TABLE Appointment (
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Notes varchar(2000)
	)

CREATE TABLE Client (
	Username varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	phone varchar(50) NOT NULL,
	Type varchar(50) NOT Null,
	FOREIGN KEY (Type) REFERENCES Type
	)

CREATE TABLE Timeslot (
	Id int IDENTITY(1,1) NOT NULL,
	Appointment_Id int NOT NULL UNIQUE,
	Time varchar(50) NOT NULL,
	Date varchar(50) NOT NULL,
	PRIMARY KEY (Id, Time, Date),
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)
		

CREATE TABLE Books (
	Client_Username varchar(50) NOT NULL,
	Appointment_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Appointment_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)

CREATE TABLE Receives (
	Client_Username varchar(50) NOT NULL,
	Doc_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Doc_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Doc_Id) REFERENCES Document
	)

CREATE TABLE Assigned (
	License varchar(50) NOT NULL,
	Instructor_Username varchar(50) NOT NULL,
	Appointment_Id int NOT NULL,
	Confirmed varchar(3) NOT NULL,
	PRIMARY KEY (License, Instructor_Username, Appointment_Id),
	FOREIGN KEY (License) REFERENCES Car,
	FOREIGN KEY (Instructor_Username) REFERENCES Instructor,
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)

-- Drop statements
/*
DROP TABLE Admin
DROP TABLE Type
DROP TABLE Document
DROP TABLE Appointment
DROP TABLE Car
DROP TABLE Instructor
DROP TABLE Timeslot
DROP TABLE Client
DROP TABLE Books
DROP TABLE Receives
DROP TABLE Assigned 
*/

--Insert statements
INSERT INTO Admin VALUES ('Logan00', 'LoganPW0', 'Logan', 'Walker', 'loganwalker@gmail.com')
INSERT INTO Admin VALUES ('Ben01', 'BenPW1', 'Ben', 'Lett', 'benlett@gmail.com')
INSERT INTO Admin VALUES ('Tyler02', 'TylerPW2', 'Tyler', 'Bruin', 'tylerbruin@gmail.com')


INSERT INTO Type VALUES ('Beginner', '$200', 10)
INSERT INTO Type VALUES ('Intermediate','$150', 7)
INSERT INTO Type VALUES ('Advanced', '$100', 5)


INSERT INTO Instructor VALUES ('Paul00', 'PaulPW0', 'Paul', 'Jackson', 'paul00@gmail.com', '0271112222')
INSERT INTO Instructor VALUES ('Jack01', 'JackPW1', 'Jack', 'Paterson', 'jack01@gmail.com', '0273334444')
INSERT INTO Instructor VALUES ('Jeff02', 'JeffPW2', 'Jeff', 'Jones', 'jeff02@gmail.com', '0275556666')
INSERT INTO Instructor VALUES ('Ryan03', 'RyanPW3', 'Ryan', 'Patt', 'ryan03@gmail.com', '0277778888')


INSERT INTO Car VALUES ('C4R111', 'Subaru')
INSERT INTO Car VALUES ('C4R222', 'Nissan')
INSERT INTO Car VALUES ('C4R333', 'Toyota')
INSERT INTO Car VALUES ('C4R444', 'Holden')
INSERT INTO Car VALUES ('C4R555', 'lamborghini')


INSERT INTO Appointment VALUES ('Student Requires further training')
INSERT INTO Appointment VALUES ('Student Requires further training')
INSERT INTO Appointment VALUES ('Student Requires further training')
INSERT INTO Appointment VALUES ('Student Requires further training')
INSERT INTO Appointment VALUES ('Student does not require further training')
INSERT INTO Appointment VALUES ('Student does not require further training')
INSERT INTO Appointment VALUES ('Student does not require further training')
INSERT INTO Appointment VALUES ('Student does not require further training')
INSERT INTO Appointment VALUES ('Student does not require further training')
INSERT INTO Appointment VALUES ('Student failed to indicate correctly, needs further training')
INSERT INTO Appointment VALUES ('Student failed to indicate correctly, needs further training')
INSERT INTO Appointment VALUES ('Student failed to indicate correctly, needs further training')
INSERT INTO Appointment VALUES ('Student failed to indicate correctly, needs further training')
INSERT INTO Appointment VALUES ('Student failed to indicate correctly, needs further training')
INSERT INTO Appointment VALUES ('No Notes')
INSERT INTO Appointment VALUES ('No Notes')
INSERT INTO Appointment VALUES ('No Notes')
INSERT INTO Appointment VALUES ('No Notes')
INSERT INTO Appointment VALUES ('Student has completed the course succesfully')
INSERT INTO Appointment VALUES ('Student has completed the course succesfully')
INSERT INTO Appointment VALUES ('Student has completed the course succesfully')
INSERT INTO Appointment VALUES ('Student has completed the course succesfully')
INSERT INTO Appointment VALUES ('Student has completed the course succesfully')
INSERT INTO Appointment VALUES ('An accident occured during session, student failed course')
INSERT INTO Appointment VALUES ('An accident occured during session, student failed course')
INSERT INTO Appointment VALUES ('An accident occured during session, student failed course')
INSERT INTO Appointment VALUES ('An accident occured during session, student failed course')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('')

INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (1, '8:30am', '2017/4/1')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (2, '9:00am', '2017/4/2')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (3, '9:30am', '2017/4/3')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (4, '10:00am', '2017/4/4')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (5, '10:30am', '2017/4/5')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (6, '11:30am', '2017/4/6')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (7, '12:00am', '2017/4/7')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (8, '1:30pm', '2017/4/8')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (9, '2:30pm', '2017/4/9')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (10, '3:30pm', '2017/4/10')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (11, '4:30pm', '2017/4/11')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (12, '4:30pm', '2017/4/12')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (13, '10:30am', '2017/4/13')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (14, '10:30am', '2017/4/14')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (15, '9:30am', '2017/4/15')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (16, '8:30am', '2017/4/16')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (17, '1:00pm', '2017/4/17')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (18, '9:30am', '2017/4/18')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (19, '8:30am', '2017/4/19')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (20, '8:30am', '2017/4/20')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (21, '10:30am', '2017/4/21')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (22, '9:30am', '2017/4/22')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (23, '8:30am', '2017/4/23')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (24, '10:30am', '2017/4/24')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (25, '11:30am', '2017/4/25')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (26, '11:00am', '2017/4/26')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (27, '8:00am', '2017/4/27')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (28, '9:30am', '2017/4/28')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (29, '10:00am', '2017/4/29')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (30, '11:00am', '2017/4/30')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (31, '10:30am', '2017/4/31')

-- Select Statements
/*
Select * FROM Admin
Select * FROM Type
Select * FROM Instructor
Select * FROM Car
Select * FROM Appointment
Select * FROM Timeslot

Select * FROM Client

Select * FROM Document
Select * FROM Books
Select * FROM Receives
Select * FROM Assigned 
*/